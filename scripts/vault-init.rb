#!/usr/bin/env ruby
require 'json'
require 'yaml'
require 'net/https'

API_VERSION = 'v1'

client = Net::HTTP.new('127.0.0.1', 8200)
secrets = YAML.load_file('/vagrant/secrets.yml')

def init_vault(client)
  init_uri = "/#{API_VERSION}/sys/init"
  payload = {
    'secret_shares' => 3,
    'secret_threshold' => 3
  }

  response = client.send_request('PUT', init_uri, JSON[payload])
  init_data = JSON.parse(response.body)

  puts '############### VAULT INFO ##################'
  puts 'Root Token: ' + init_data["root_token"]
  puts 'Unseal Keys:'
  init_data["keys"].each do |key|
    puts key
  end
  puts '############### VAULT INFO ##################'

  init_data
end

def unseal_vault(client, keys)
  unseal_uri = "/#{API_VERSION}/sys/unseal"
  keys.each do |key|
    payload = {
      'key' => key
    }
    response = client.send_request('PUT', unseal_uri, JSON[payload])
    puts response.body
  end
end

def write_secrets(client, token, secrets)
  header = {
    'X-Vault-Token' => token
  }
  secrets.each do |backend, paths|
    backend_uri =  "/#{API_VERSION}/#{backend}"
    paths.each do |path, data|
      payload = JSON[data]
      client.send_request('POST', "#{backend_uri}/#{path}", payload, header)
    end
  end
end

def verify_secrets(client, token, secrets)
  header = {
    'X-Vault-Token' => token
  }
  secrets.each do |backend, paths|
    backend_uri =  "/#{API_VERSION}/#{backend}"
    paths.each do |path, data|
      response = client.send_request('GET', "#{backend_uri}/#{path}", nil, header)
      puts response.body
    end
  end
end

init_data = init_vault(client)
unseal_vault(client, init_data["keys"])
write_secrets(client, init_data["root_token"], secrets)
verify_secrets(client, init_data["root_token"], secrets)
