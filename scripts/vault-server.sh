#!/usr/bin/env bash
yum install -y unzip vim net-tools ruby

# Install vault binary and setup user
useradd -M -s /sbin/nologin vault
mkdir /etc/vault
cp /vagrant/config/vault/server /etc/vault
mkdir -p /opt/vault/bin
unzip /vagrant/artifacts/vault_${1}_linux_amd64.zip -d /opt/vault/bin
chown -R vault:vault /opt/vault

cp /vagrant/config/systemd/vault.service /etc/systemd/system/
chmod 664 /etc/systemd/system/vault.service

systemctl start vault.service
