# -*- mode: ruby -*-
# vi: set ft=ruby :

# Ensure required plugins are installed
required_plugins = %w(vagrant-triggers nokogiri)

plugins_to_install = required_plugins.select \
  { |plugin| not Vagrant.has_plugin? plugin }
if not plugins_to_install.empty?
  puts "Installing plugins: #{plugins_to_install.join(' ')}"
  if system "vagrant plugin install #{plugins_to_install.join(' ')}"
    exec "vagrant #{ARGV.join(' ')}"
  else
    abort "Installation of one or more plugins has failed. Aborting."
  end
end

require 'nokogiri'
require 'open-uri'
require 'digest'

CONFIG = File.join(File.dirname(__FILE__), "config.rb")
VAULT_RELEASE_URL = 'https://releases.hashicorp.com/vault/'

# Defaults for config options defined in CONFIG
$vault_version = 'latest'
$vm_memory = 1024
$vm_cpus = 1
$shared_folders = {}
$forwarded_ports = {8200 => 8200}

if File.exist?(CONFIG)
  require CONFIG
end

def get_latest_vault_version
  doc = Nokogiri::HTML(open(VAULT_RELEASE_URL))
  doc.xpath("//li//a").children.drop(1)[0].to_s.split("_")[1]
end

if $vault_version == 'latest'
  $vault_version = get_latest_vault_version
end

def download_vault(version)
  puts "Downloading vault_#{version}_linux_amd64.zip..."
  sums = "#{VAULT_RELEASE_URL}#{version}/vault_#{version}_SHA256SUMS"
  archive = "#{VAULT_RELEASE_URL}#{version}/vault_#{version}_linux_amd64.zip"
  File.open("artifacts/vault_#{version}_linux_amd64.zip",'wb') {
    |f| f.write(open(archive).read)
  }

  archive_sum = Digest::SHA256.file "artifacts/vault_#{version}_linux_amd64.zip"

  doc = Nokogiri::HTML(open(sums))
  good_sum = (doc.xpath("//body//p").children.to_s.lines("\n").select {
    |l| l =~ /linux_amd64/
  }).join(" ").split[0]

  if archive_sum != good_sum
    puts "ERROR: Checksum mismatch," \
      " removing \'artifacts/vault_#{version}_linux_amd64.zip\'"
    File.delete("artifacts/vault_#{version}_linux_amd64.zip")
  end
end
if ARGV[0].eql?('up')
  download_vault($vault_version) unless \
    File.exist?("artifacts/vault_#{$vault_version}_linux_amd64.zip")
end
Vagrant.configure(2) do |config|
  config.vm.provider :virtualbox do |vb|
    vb.memory = $vm_memory
    vb.cpus = $vm_cpus
  end

  config.vm.define "vault" do |vault|
    vault.vm.box = "centos/7"
    vault.vm.hostname = "vault.lab.outerhaven.co"
    vault.vm.provision :shell, \
      path: "scripts/vault-server.sh", args: $vault_version
    $forwarded_ports.each do |guest, host|
      vault.vm.network "forwarded_port", \
        guest: guest, host: host, auto_correct: true
    end
    vault.trigger.after :up do
      info "Initializing vault..."
      run_remote "ruby /vagrant/scripts/vault-init.rb"
    end
  end
end
